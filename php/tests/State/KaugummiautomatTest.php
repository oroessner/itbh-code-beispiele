<?php
declare(strict_types=1);

namespace App\Test\State;

use App\State\AusverkauftZustand;
use App\State\HatMuenzeZustand;
use App\State\Kaugummiautomat;
use App\State\KeineMuenzeZustand;
use PHPUnit\Framework\TestCase;

final class KaugummiautomatTest extends TestCase
{
    private Kaugummiautomat $automat;

    /**
     * @test
     */
    public function werfeMuenzeEinWechseltZustandAufHatMuenze(): void
    {
        $this->automat->muenzeEinwerfen();

        $this->assertAutomatenZustand(HatMuenzeZustand::class);
    }

    /**
     * @test
     */
    public function initialerStatusIstKeineMuenze(): void
    {
        $this->assertAutomatenZustand(KeineMuenzeZustand::class);
    }

    /**
     * @test
     */
    public function initialerZustandIstAusverkauftWennKeineKugelnBefuellt(): void
    {
        $this->automat = new Kaugummiautomat(0);
        $this->assertAutomatenZustand(AusverkauftZustand::class);
    }

    /**
     * @test
     */
    public function muenzeReinMuenzeRausIstKeineMuenze(): void
    {
        $this->automat->muenzeEinwerfen();
        $this->automat->muenzeAuswerfen();

        $this->assertAutomatenZustand(KeineMuenzeZustand::class);
    }

    /**
     * @test
     */
    public function muenzeReinBeiMuenzeDrinAendertZustandNicht(): void
    {
        $this->expectOutputRegex('/Sie können keine weitere Münze einwerfen/');

        $this->automat->muenzeEinwerfen();
        $this->automat->muenzeEinwerfen();

        $this->assertAutomatenZustand(HatMuenzeZustand::class);
    }

    protected function setUp(): void
    {
        $this->automat = new Kaugummiautomat(3);
        parent::setUp();
    }

    private function assertAutomatenZustand(string $stateClass): void
    {
        self::assertInstanceOf($stateClass, $this->automat->getZustand());
    }

}
