<?php
declare(strict_types=1);

namespace App\Test\Rechnung;

use App\Rechnung\RechnungsErsteller;
use App\Rechnung\SystemHausInterface;
use PHPUnit\Framework\TestCase;

final class RechnungsErstellerTest extends TestCase
{
    /**
     * @test
     */
    public function getKundenIds(): void
    {
        // Arrange
        $kId1  = 'K00091';
        $kId2  = 'K01234';
        $satz1 = '<satz-1>';
        $satz2 = '<satz-2>';

        $expected = [
          $kId1,
          $kId2,
        ];

        $systemhaus = $this->prophesize(SystemHausInterface::class);

        $systemhaus->hole_journalsatz()
            ->shouldBeCalledTimes(3)
            ->willReturn($satz1, $satz2, '');

        $systemhaus->lese_kundenid($satz1)
            ->shouldBeCalled()
            ->willReturn($kId1);

        $systemhaus->lese_kundenid($satz2)
            ->shouldBeCalled()
            ->willReturn($kId2);

        // Act
        $rechnungsErsteller = new RechnungsErsteller($systemhaus->reveal());
        // Assert
        self::assertSame($expected, $rechnungsErsteller->getKundenIds());
    }
}
