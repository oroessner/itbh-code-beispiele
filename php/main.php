<?php
declare(strict_types=1);

use App\ExternerMitarbeiter;
use App\Mitarbeiter;

require_once __DIR__.'/vendor/autoload.php';

$max    = new Mitarbeiter('Max', 123);
$moritz = new ExternerMitarbeiter('Moritz', 234);

$max->anzeigen();
$moritz->anzeigen();
