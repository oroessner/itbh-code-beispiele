<?php
declare(strict_types=1);

use App\Logger\EchoLogger;
use App\Observer\Calculator;
use App\Observer\LoggingObserver;
use App\Observer\ObservableCalculator;

require_once __DIR__.'/vendor/autoload.php';

$calculator        = new Calculator();
$logger            = new EchoLogger();
$observerCalulator = new ObservableCalculator($calculator);

$observer1 = new LoggingObserver($logger);
$observer2 = new LoggingObserver($logger);

$observerCalulator->addObserver($observer1);
$observerCalulator->addObserver($observer2);

$observerCalulator->add(3, 5);
