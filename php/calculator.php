<?php
declare(strict_types=1);

use App\Calculator\AbstractCalculator;
use App\Calculator\Calculator;
use App\Calculator\DICalculator;
use App\Calculator\DICalculatorWithVariableLogger;
use App\Logger\EchoLogger;
use App\Logger\FileLogger;
use App\Logger\ReverseLogger;

require_once __DIR__.'/vendor/autoload.php';

function writeLn(string $text)
{
    echo "$text\n";
}

function echoCalculation(AbstractCalculator $calculator)
{
    $a = 2;
    $b = 5;

    echo sprintf("%d + %d = %d\n", $a, $b, $calculator->add($a, $b));
}

$calculator = new Calculator();

writeLn('Regular Calculator');
echoCalculation($calculator);

writeLn('DI Calculator');
echoCalculation(new DICalculator(new EchoLogger()));

$variableLogger = new DICalculatorWithVariableLogger();
writeLn('DI Calculator with NullLogger');
echoCalculation($variableLogger);

$variableLogger->setLogger(new EchoLogger());
writeLn('DI Calculator with EchoLogger');
echoCalculation($variableLogger);

$variableLogger->setLogger(new ReverseLogger());
writeLn('DI Calculator with ReverseLogger');
echoCalculation($variableLogger);

$variableLogger->setLogger(new FileLogger(__DIR__.'/log.txt'));
writeLn('DI Calculator with FileLogger');
echoCalculation($variableLogger);
