<?php
declare(strict_types=1);

namespace App;
/**
 * Class ExternerMitarbeiter "is-a" Mitarbeiter && "has-a" Personalnummer
 */
final class ExternerMitarbeiter extends Mitarbeiter
{
    public function __construct(string $name, int $personalNummer)
    {
        parent::__construct($name, $personalNummer);
        $this->personalNummer = new ExternePersonalnummer($personalNummer);
    }
}
