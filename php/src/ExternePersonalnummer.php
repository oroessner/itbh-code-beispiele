<?php
declare(strict_types=1);

namespace App;

/**
 * Class ExternePersonalnummer
 *
 * @package App
 */
final class ExternePersonalnummer extends Personalnummer
{
    public function getPersonalNummer(): int
    {
        return $this->personalNummer + 10000;
    }
}
