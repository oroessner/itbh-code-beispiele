<?php
declare(strict_types=1);

namespace App;
/**
 * Class Kunde
 */
class Kunde
{
    private string $vorname;

    private string $nachname;

    private string $adresse;

    private string $email;

    /**
     * Kunde constructor.
     *
     * @param string $vorname
     */
    public function __construct(string $vorname)
    {
        $this->vorname = $vorname;
    }

    /**
     * @return string
     */
    public function getVorname(): string
    {
        return $this->vorname;
    }

    /**
     * @return string
     */
    public function getAdresse(): string
    {
        return $this->adresse;
    }

    public function isMitarbeiter(): bool
    {
        return false;
    }

}
