Aufgabe 5
==========

Gegeben sei:

* Liste `Matches` mit gespielten Matches.
* Leeres Konstrukt TableLeague.

```
Erzeuge Konstrukt TeamStatistik mit Eigenschaften

 * Team
 * Punkte
 * Tordifferenz

var statistiken: List<TeamStatistik>

für alle Matches als Match {
    var gewinner: Team
    var verlierer: Team
    var tordifferenz: int = 0

    wenn Match.homeScore größer Match.guestScore {
        setze gewinner = Match.home
        setze verlierer = Match.guest
        setze tordifferenz = Match.homeScore - Match.guestScore
    }
    wenn Match.guestScore größer Match.homeScore {
        setze gewinner = Match.guest
        setze verlierer = Match.home
        setze tordifferenz = Match.guestScore - Match.homeScore
    }

    wenn gewinner und verlierer gesetzt {

        für gewinner und verlierer als team durchlaufe {

            var statistik: TeamStatistik

            wenn statistiken einen Eintrag für team hat {
                setze statistik = statistiken[team]
            }
            sonst {
                erzeuge TeamStatistik für team mit Punkte = 0 und Tordifferenz = 0
                setze statistik auf die erzeugte TeamStatistik
                füge statistiken die erzeugte TeamStatistik hinzu
            }

            wenn team == gewinner {
                addiere 2 zu statistik.Punkte 
            }

            wenn team == verlierer {
                subtrahiere 2 zu statistik.Punkte
            }

            addiere tordifferenz zu statistik.Tordifferenz
        }
        
    }
}

// hier fehlt noch was...
sortiere statistiken natürlich nach Punkte absteigend 

für alle statistiken als statistik {
 füge TableLeague einen Eintrag hinzu mit statistik.Team, statistik.Punkte und statistik.Tordifferenz
}

```
