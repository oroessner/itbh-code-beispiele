<?php
declare(strict_types=1);

namespace App;
/**
 * Class Hotel
 */
final class Hotel
{
    private string $name;

    private string $ort;

    private array $ausstattung;

    private bool $isFrei = true;

    public function close()
    {
        if (!$this->isFrei) {
            throw new HotelException('Hotel ist schon zu!');
        }
        $this->isFrei = false;
    }

    public function open()
    {
        $this->isFrei = true;
    }

    /**
     * @return bool
     */
    public function isFrei(): bool
    {
        return $this->isFrei;
    }
}
