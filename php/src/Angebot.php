<?php
declare(strict_types=1);

namespace App;
/**
 * Class Angebot
 */
final class Angebot
{
    private Hotel $hotel;

    private int $sterne;

    private string $zimmertyp;

    /**
     * @return \Hotel
     */
    public function getHotel(): \Hotel
    {
        return $this->hotel;
    }

    /**
     * @return int
     */
    public function getSterne(): int
    {
        return $this->sterne;
    }

    /**
     * @return string
     */
    public function getZimmertyp(): string
    {
        return $this->zimmertyp;
    }

}
