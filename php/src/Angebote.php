<?php
declare(strict_types=1);

namespace App;
/**
 * Class Angebote
 */
final class Angebote
{
    /**
     * @var array|Angebot[]
     */
    private array $list;

    /**
     * @return array|Angebot[]
     */
    public function all():array
    {
        return $this->list;
    }
}
