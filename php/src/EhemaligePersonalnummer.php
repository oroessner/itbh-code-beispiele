<?php
declare(strict_types=1);

namespace App;

/**
 * Class EhemaligePersonalnummer
 *
 * @package App
 */
final class EhemaligePersonalnummer extends Personalnummer
{
    public function getPersonalNummer(): int
    {
        return $this->personalNummer * -1;
    }
}
