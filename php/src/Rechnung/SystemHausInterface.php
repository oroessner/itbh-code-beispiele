<?php
declare(strict_types=1);

namespace App\Rechnung;

interface SystemHausInterface
{
    public function hole_journalsatz(): string;

    public function lese_kundenid(string $satz): string;

    public function lese_leistungid(string $satz): int;

    public function lese_einzelpreis(string $satz): float;

    public function lese_anzahl(string $satz): int;

    public function hole_bezeichnung(int $leistungsId): string;

    public function schreibe_kopfzeile(): void;

    public function schreibe_positionszeile(
      int $pos,
      int $leistungsId,
      string $bezeichnung,
      int $anzahl,
      float $einzelpreis,
      float $gesamtpreis
    ): void;


    public function schreibe_rechnungssumme(float $rechnungssumme): void;
}
