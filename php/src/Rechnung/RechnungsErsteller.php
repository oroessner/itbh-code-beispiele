<?php
declare(strict_types=1);

namespace App\Rechnung;

/**
 * Class RechnungsErsteller
 *
 * @package App\Test\Rechnung
 */
final class RechnungsErsteller
{
    private SystemHausInterface $systemhaus;

    public function __construct(SystemHausInterface $systemhaus)
    {
        $this->systemhaus = $systemhaus;
    }

    public function getKundenIds():array
    {
        $kunden = [];

        $satz = $this->systemhaus->hole_journalsatz();
        while ($satz !== '') {
            $kunden[] = $this->systemhaus->lese_kundenid($satz);

            $satz = $this->systemhaus->hole_journalsatz();
        }

        return $kunden;
    }
}
