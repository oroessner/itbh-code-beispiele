erstelleRechnung()
===================

```
var kunden = []
var journal = []

SOLANGE satz := hole_journalsatz() UNGLEICH ""
    journal.add(satz)
    WENN kundenId := lese_kundenid(satz) NICHT IN kunden
        kunden.add(kundenId)
    ENDE WENN
ENDE SOLANGE


FÜR JEDE kundenId IN kunden
    var leistungen = []{
        id: int
        einzelPreis: double
        anzahl: int
    }
    FÜR JEDEN satz IN journal 
        WENN kundenId GLEICH lese_kundenid(satz)
            
            WENN leistungsId := lese_leistungsid(satz) NICHT IN leistungen
                leistungen.add(leistungsId, lese_einzelpreis(satz), lese_anzahl(satz))
            SONST
                leistungen[leistungsId].anzahl += lese_anzahl(satz)
            ENDE WENN
        ENDE WENN
    ENDE FÜR JEDEN

    var position = 1
    var summe = 0.0

    schreibe_kundenid(kundenid)
    schreibe_kopfzeile()
    FÜR JEDE leistungs IN leistungen
        var positionsSumme = leistung.anzahl * leistung.einzelPreis
        schreibe_positionszeile(position++, leistungs.id, hole_bezeichnung(leistungs.id), leistung.anzahl, 
                                leistung.einzelPreis, positionsSumme)
        summe += positionsSumme
    ENDE FÜR JEDE 
    schreibe_rechnungssumme(summe)
    
ENDE FÜR JEDE
```
