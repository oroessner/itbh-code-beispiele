<?php
declare(strict_types=1);

namespace App\Hangman;


/**
 * Class ErrorCountingHangman
 */
final class ErrorCountingHangman implements HangmanInterface
{
    private HangmanInterface $delegate;

    public function __construct(HangmanInterface $hangman)
    {
        $this->delegate = $hangman;
    }

    public function getNumErrors(): int
    {

    }

    /**
     * @inheritDoc
     */
    public function guessLetter(string $letter): string
    {
        return $this->delegate->guessLetter($letter);
    }
}
