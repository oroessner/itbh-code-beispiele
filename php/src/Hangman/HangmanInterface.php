<?php
declare(strict_types=1);

namespace App\Hangman;

/**
 * Class HangmanInterface
 */
interface HangmanInterface
{
    /**
     * Rate Buchstabe und dann gibt es das gesuchte Wort mit den ersetzten Buchstaben zurück
     *
     * @example
     * $hangman = new Hangman("Developer");
     * $hangman->guessLetter("x") => "---------"
     * $hangman->guessLetter("e") => "-e-e---e-"
     */
    public function guessLetter(string $letter): string;
}
