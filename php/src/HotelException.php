<?php
declare(strict_types=1);

namespace App;
/**
 * Class HotelException
 */
final class HotelException extends \RuntimeException
{
}
