<?php
declare(strict_types=1);

namespace App;

/**
 * Class MitarbeiterPersonalnummer
 *
 * @package App
 */
final class MitarbeiterPersonalnummer extends Personalnummer
{
    public function getPersonalNummer(): int
    {
        return $this->personalNummer;
    }
}
