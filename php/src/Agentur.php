<?php
declare(strict_types=1);

namespace App;

final class Agentur
{
    private Logger $logger;

    /**
     * Agentur constructor.
     */
    public function __construct()
    {
        $this->logger = new Logger();
    }

    public function gibAngebote(Anfrage $anfrage, Kunde $kunde): Angebote
    {
        // Pseudo Überladung in PHP :P
        switch (get_class($kunde)) {
            case Stammkunde::class:
                return $this->gibAngeboteFürStammkunden($anfrage, $kunde);
            case Mitarbeiter::class:
                return $this->gibAngeboteFürMitarbeiter($anfrage, $kunde);
        }

        $this->logger->log('gib Angebote für Kunde');

        return $this->generateAngebote();
    }

    private function gibAngeboteFürStammkunden(Anfrage $anfrage, Stammkunde $stammkunde): Angebote
    {
        $this->logger->log('gib Angebote für Stammkunden');

        return $this->generateAngebote();

    }

    private function generateAngebote(): \Angebote
    {
        return new Angebote();
    }

    private function gibAngeboteFürMitarbeiter(Anfrage $anfrage, Mitarbeiter $mitarbeiter): Angebote
    {
        $this->logger->log('gib Angebote für Mitarbeiter');

        return $this->generateAngebote();
    }
}
