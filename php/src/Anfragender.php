<?php
declare(strict_types=1);

namespace App;

/**
 * Class Anfragender
 */
final class Anfragender
{
    public string $uid;
}
