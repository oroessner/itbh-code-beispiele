<?php
declare(strict_types=1);

namespace App\Observer;

/**
 * Class ObservableCalculator
 *
 * @package App\Observer
 */
final class ObservableCalculator implements CalculatorInterface
{
    private CalculatorInterface $delegate;

    /**
     * @var array|ResultAwareInterface[]
     */
    private array $observer = [];

    public function __construct(CalculatorInterface $calculator)
    {
        $this->delegate = $calculator;
    }

    public function addObserver(ResultAwareInterface $observer)
    {
        $this->observer[] = $observer;
    }

    public function add(int $a, int $b): int
    {
        $result = $this->delegate->add($a, $b);

        $this->notifyObservers($result);

        return $result;
    }

    private function notifyObservers(int $value)
    {
        $result = new Result($value);
        foreach ($this->observer as $observer) {
            $observer->setResult($result);
        }
    }

    public function substract(int $a, int $b): int
    {
        $result = $this->delegate->substract($a, $b);

        $this->notifyObservers($result);

        return $result;
    }
}
