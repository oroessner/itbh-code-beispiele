<?php
declare(strict_types=1);

namespace App\Observer;

/**
 * Class Calculator
 */
final class Calculator implements CalculatorInterface
{
    public function add(int $a, int $b): int
    {
        return $a + $b;
    }


    public function substract(int $a, int $b): int
    {
        return $a - $b;
    }
}
