<?php
declare(strict_types=1);

namespace App\Observer;


/**
 * Class Calculator
 */
interface CalculatorInterface
{
    public function add(int $a, int $b): int;

    public function substract(int $a, int $b): int;
}
