<?php
declare(strict_types=1);

namespace App\Observer;

use App\Logger\LoggerInterface;

/**
 * Class LoggingObserver
 *
 * @package App\Observer
 */
final class LoggingObserver implements ResultAwareInterface
{
    /**
     * @var \App\Logger\LoggerInterface
     */
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function setResult(Result $result): void
    {
        $this->logger->log(sprintf("Result: %d", $result->value));
    }
}
