<?php
declare(strict_types=1);

namespace App\Observer;

/**
 * Class ResultAwareInterface
 */
interface ResultAwareInterface
{
    public function setResult(Result $result): void;
}
