<?php
declare(strict_types=1);

namespace App\Observer;


/**
 * Class Result
 */
final class Result
{
    public int $value;

    public function __construct(int $value)
    {
        $this->value = $value;
    }

}
