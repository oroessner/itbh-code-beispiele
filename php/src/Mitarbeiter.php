<?php
declare(strict_types=1);

namespace App;

/**
 * Class Mitarbeiter
 */
class Mitarbeiter extends Kunde
{
    protected Personalnummer $personalNummer;

    /**
     * Mitarbeiter constructor.
     *
     * @param int $personalNummer
     */
    public function __construct(string $name, int $personalNummer)
    {
        parent::__construct($name);
        $this->personalNummer = new MitarbeiterPersonalnummer($personalNummer);
    }

    public function isMitarbeiter(): bool
    {
        return true;
    }

    public function anzeigen(): void
    {
        echo $this->getVorname().': '.$this->getPersonalNummer()."\n";
    }

    /**
     * @return int
     */
    public function getPersonalNummer(): int
    {
        return $this->personalNummer->getPersonalNummer();
    }
}
