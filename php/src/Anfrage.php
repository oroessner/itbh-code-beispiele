<?php
declare(strict_types=1);

namespace App;

/**
 * Class Anfrage
 */
final class Anfrage
{
    private Anfragender $anfragender;

    private Gebiet $gebiet;

    private Zeitraum $zeitraum;

    /**
     * @return \Anfragender
     */
    public function getAnfragender(): \Anfragender
    {
        return $this->anfragender;
    }

    /**
     * @return \Gebiet
     */
    public function getGebiet(): \Gebiet
    {
        return $this->gebiet;
    }

    /**
     * @return \Zeitraum
     */
    public function getZeitraum(): \Zeitraum
    {
        return $this->zeitraum;
    }
}
