<?php
declare(strict_types=1);

namespace App\Logger;

/**
 * Class LoggerInterface
 *
 * @package Calculator
 */
interface LoggerInterface
{
    public function log(string $text): void;
}
