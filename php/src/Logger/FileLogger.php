<?php
declare(strict_types=1);

namespace App\Logger;

/**
 * Class FileLogger
 *
 * @package App\Logger
 */
final class FileLogger implements LoggerInterface
{
    private string $path;

    public function __construct(string $path)
    {
        $this->path = $path;
    }

    public function log(string $text): void
    {
        $fileHandle = fopen($this->path, 'w+');
        fwrite($fileHandle, $text);
        fclose($fileHandle);
    }
}
