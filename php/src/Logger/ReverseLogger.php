<?php
declare(strict_types=1);

namespace App\Logger;

/**
 * Class ReverseLogger
 *
 * @package Logger
 */
final class ReverseLogger implements LoggerInterface
{
    public function log(string $text): void
    {
        $text = strrev($text);
        echo "$text\n";
    }
}
