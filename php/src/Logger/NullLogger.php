<?php
declare(strict_types=1);

namespace App\Logger;

/**
 * Class NullLogger
 *
 * @package Logger
 */
final class NullLogger implements LoggerInterface
{
    public function log(string $text): void
    {
        // /dev/null
    }
}
