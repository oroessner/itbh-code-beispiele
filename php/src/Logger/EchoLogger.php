<?php
declare(strict_types=1);

namespace App\Logger;

/**
 * Class EchoLogger
 *
 * @package Logger
 */
final class EchoLogger implements LoggerInterface
{
    public function log(string $text): void
    {
        echo "$text\n";
    }
}
