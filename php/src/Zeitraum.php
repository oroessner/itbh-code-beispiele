<?php
declare(strict_types=1);

namespace App;

/**
 * Class Zeitraum
 */
final class Zeitraum
{
    public \DateTimeInterface $from;

    public \DateTimeInterface $to;

    /**
     * Zeitraum constructor.
     *
     * @param \DateTimeInterface $from
     * @param \DateTimeInterface $to
     */
    public function __construct(\DateTimeInterface $from, \DateTimeInterface $to)
    {
        $this->from = $from;
        $this->to   = $to;
    }
}
