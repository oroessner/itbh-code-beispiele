<?php
declare(strict_types=1);

namespace App;

/**
 * Class Personalnummer
 *
 * @package App
 */
abstract class Personalnummer implements HatPersonalnummer
{
    protected int $personalNummer;

    /**
     * MitarbeiterPersonalnummer constructor.
     *
     * @param int $personalNummer
     */
    public function __construct(int $personalNummer)
    {
        $this->personalNummer = $personalNummer;
    }
}
