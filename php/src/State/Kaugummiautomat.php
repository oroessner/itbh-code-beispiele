<?php
declare(strict_types=1);

namespace App\State;

use App\Logger\EchoLogger;

/**
 * Class Kaugummiautomat
 *
 * @package App\State
 */
final class Kaugummiautomat implements IZustand, IAutomat
{
    private int $anzahl;

    private IZustand $zustand;

    private $logger;

    /**
     * Kaugummiautomat constructor.
     *
     * @param int $anzahl
     */
    public function __construct(int $anzahl)
    {
        $this->logger = new EchoLogger();

        $this->zustand = $this->getAusverkauftZustand();

        $this->anzahl = $anzahl;

        if ($anzahl > 0) {
            $this->zustand = $this->getKeineMuenzeZustand();
        }
    }

    public function getKeineMuenzeZustand(): KeineMuenzeZustand
    {
        return new KeineMuenzeZustand($this->logger, $this);
    }

    public function getHatMuenzeZustand(): HatMuenzeZustand
    {
        return new HatMuenzeZustand($this->logger, $this);
    }

    public function getVerkauftZustand(): VerkauftZustand
    {
        return new VerkauftZustand($this->logger, $this);
    }

    public function getAusverkauftZustand(): AusverkauftZustand
    {
        return new AusverkauftZustand($this->logger, $this);
    }

    public function getGewinnZustand()
    {
        return new GewinnZustand($this->logger, $this);
    }

    public function muenzeEinwerfen(): void
    {
        $this->zustand->muenzeEinwerfen();
    }

    public function griffDrehen(): void
    {
        $this->zustand->griffDrehen();
        $this->zustand->kugelAusgeben();
    }

    public function kugelAusgeben(): void
    {
        $this->zustand->kugelAusgeben();
    }

    public function muenzeAuswerfen(): void
    {
        $this->zustand->muenzeAuswerfen();
    }

    public function reduziereAnzahl(): void
    {
        $this->anzahl--;
    }

    public function __toString(): string
    {
        return <<<STR

Kaukugel & Co. KG Automat
Bestand: {$this->anzahl} Kaugummikugeln
{$this->zustand}

STR;

    }

    public function getZustand(): IZustand
    {
        return $this->zustand;
    }

    /**
     * @param \App\State\IZustand|int $zustand
     */
    public function setZustand(IZustand $zustand): void
    {
        $this->zustand = $zustand;
    }

    /**
     * @return int
     */
    public function getAnzahl(): int
    {
        return $this->anzahl;
    }

    private function hatZustand(int $zustand): bool
    {
        return $this->zustand === $zustand;
    }

    private function log(string $text): void
    {
        echo "$text\n";
    }

    private function gewinnKugelAusgeben(): void
    {
        $try = random_int(0, 1) * 100;
        if ($try / 10 <= 10) {
            $this->kugelAusgeben();
        }
    }
}
