<?php
declare(strict_types=1);

namespace App\State;

/**
 * Class KeineMuenzeZustand
 *
 * @package App\State
 */
final class KeineMuenzeZustand extends AbstractZustand
{
    public function muenzeEinwerfen(): void
    {
        $this->automat->setZustand($this->automat->getHatMuenzeZustand());
        $this->logger->log('Sie haben eine Münze eingeworfen');
    }

    public function griffDrehen(): void
    {
        $this->logger->log('Sie haben gedreht, aber es ist keine Münze da');
    }

    public function kugelAusgeben(): void
    {
        $this->logger->log('Sie müssen zuerst bezahlen!');
    }

    public function muenzeAuswerfen(): void
    {
        $this->logger->log('Sie haben keine Münze eingeworfen');
    }

    public function __toString(): string
    {
        return 'Automat bereit für Münzeinwurf';
    }


}
