<?php
declare(strict_types=1);

namespace App\State;


/**
 * Class Kaugummiautomat
 *
 * @package App\State
 */
interface IZustand
{
    public function muenzeEinwerfen(): void;

    public function griffDrehen(): void;

    public function kugelAusgeben(): void;

    public function muenzeAuswerfen(): void;
}
