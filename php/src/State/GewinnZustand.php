<?php
declare(strict_types=1);

namespace App\State;

/**
 * Class GewinnZustand
 *
 * @package App\State
 */
final class GewinnZustand extends AbstractZustand
{
    public function muenzeEinwerfen(): void
    {
        $this->log('Bitte warten Sie, Sie erhalten eine Kugel');
    }

    public function griffDrehen(): void
    {
        $this->log('Auch wenn Sie zweimal drehen, bekommen Sie keine zweite Kugel');
    }

    public function kugelAusgeben(): void
    {
        $this->log('SIE HABEN GEWONNEN! Sie bekommen zwei Kugeln für ihr Geld!');
        $this->automat->reduziereAnzahl();

        if ($this->automat->getAnzahl() === 0) {
            $this->logger->log('Hoppla, keine Kugeln da!');
            $this->automat->setZustand($this->automat->getAusverkauftZustand());
        } else {
            $this->automat->reduziereAnzahl();
            if ($this->automat->getAnzahl() > 0) {
                $this->automat->setZustand($this->automat->getKeineMuenzeZustand());
            } else {
                $this->logger->log('Hoppla, keine Kugeln da!');
                $this->automat->setZustand($this->automat->getAusverkauftZustand());
            }
        }

    }

    public function muenzeAuswerfen(): void
    {
        $this->log('Zu spät, leider haben Sie en Griff schon gedreht');
    }

    public function __toString(): string
    {
        return '*** Extra Kugel gewonnen! ***';
    }
}
