<?php
declare(strict_types=1);

namespace App\State;

/**
 * Class HatMuenzeZustand
 *
 * @package App\State
 */
final class HatMuenzeZustand extends AbstractZustand
{

    public function muenzeEinwerfen(): void
    {
        $this->logger->log('Sie können keine weitere Münze einwerfen');
    }

    public function griffDrehen(): void
    {
        $this->logger->log('Sie haben den Griff gedreht...');

        $gewinn = random_int(0, 10);
        if ($gewinn === 0 && ($this->automat->getAnzahl() > 1)) {
            $this->automat->setZustand($this->automat->getGewinnZustand());
        } else {
            $this->automat->setZustand($this->automat->getVerkauftZustand());
        }

    }

    public function kugelAusgeben(): void
    {
        $this->logger->log('Es wird keine Kugel ausgegeben');
    }

    public function muenzeAuswerfen(): void
    {
        $this->logger->log('Münze wird zurückgegeben');
        $this->automat->setZustand($this->automat->getKeineMuenzeZustand());
    }

    public function __toString(): string
    {
        return 'Automat hat Münze. Jetzt drehen...';
    }
}
