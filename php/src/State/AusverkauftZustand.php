<?php
declare(strict_types=1);

namespace App\State;

/**
 * Class AusverkauftZustand
 *
 * @package App\State
 */
final class AusverkauftZustand extends AbstractZustand
{

    public function muenzeEinwerfen(): void
    {
        $this->log('Sie können keine Münze einwerfen, Automat ist ausverkauft');
    }

    public function griffDrehen(): void
    {
        $this->log('Sie haben gedrehtm aber es sind keine Kugeln da');
    }

    public function kugelAusgeben(): void
    {
        $this->log('Es wird keine Kugel ausgegeben');
    }

    public function muenzeAuswerfen(): void
    {
        $this->log('Auswurf nicht möglich, Sie haben kene Münze eingeworfen');
    }

    public function __toString(): string
    {
        return 'Automat ausverkauft';
    }
}
