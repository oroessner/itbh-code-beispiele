<?php
declare(strict_types=1);

namespace App\State;


/**
 * Class Kaugummiautomat
 *
 * @package App\State
 */
interface IAutomat
{
    public function getZustand(): IZustand;

    public function setZustand(IZustand $zustand): void;

    public function getKeineMuenzeZustand(): KeineMuenzeZustand;

    public function getHatMuenzeZustand(): HatMuenzeZustand;

    public function getVerkauftZustand(): VerkauftZustand;

    public function getAusverkauftZustand(): AusverkauftZustand;

    public function reduziereAnzahl(): void;

    public function getAnzahl(): int;

    public function getGewinnZustand();

}
