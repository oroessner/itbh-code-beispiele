<?php
declare(strict_types=1);

namespace App\State;

/**
 * Class VerkauftZustand
 *
 * @package App\State
 */
final class VerkauftZustand extends AbstractZustand
{

    public function muenzeEinwerfen(): void
    {
        $this->logger->log('Bitte warten Sie, Sie erhalten eine Kugel');
    }

    public function griffDrehen(): void
    {
        $this->logger->log('Auch wenn Sie zweimal drehen, bekommen Sie keine zweite Kugel');
    }

    public function kugelAusgeben(): void
    {
        $this->logger->log('Eine Kugel rollt aus dem Ausgabeschacht');
        $this->automat->reduziereAnzahl();

        if ($this->automat->getAnzahl() === 0) {
            $this->logger->log('Hoppla, keine Kugeln da!');
            $this->automat->setZustand($this->automat->getAusverkauftZustand());
        } else {
            $this->automat->setZustand($this->automat->getKeineMuenzeZustand());
        }
    }

    public function muenzeAuswerfen(): void
    {
        $this->logger->log('Zu spät, leider haben Sie en Griff schon gedreht');
    }

    public function __toString(): string
    {
        return 'Kugel verkauft';
    }
}
