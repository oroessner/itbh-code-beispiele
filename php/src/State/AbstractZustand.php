<?php
declare(strict_types=1);

namespace App\State;

use App\Logger\LoggerInterface;

/**
 * Class AbstractZustand
 *
 * @package App\State
 */
abstract class AbstractZustand implements IZustand
{

    /**
     * @var \App\Logger\LoggerInterface
     */
    protected LoggerInterface $logger;

    /**
     * @var \App\State\IAutomat
     */
    protected IAutomat $automat;

    public function __construct(LoggerInterface $logger, IAutomat $automat)
    {
        $this->logger  = $logger;
        $this->automat = $automat;
    }

    protected function log(string $text): void
    {
        $this->logger->log($text);
    }
}
