<?php
declare(strict_types=1);

namespace App\Calculator;

use App\Logger\LoggerInterface;

/**
 * Class AbstractCalculator
 *
 * @package App\Calculator
 */
abstract class AbstractCalculator implements CalculatorInterface
{
    protected LoggerInterface $logger;

    public function add(int $a, int $b): int
    {
        $this->logger->log("Add $a and $b");

        return $a + $b;
    }

    public function substract(int $a, int $b): int
    {
        $this->logger->log("Substract $b from $a");

        return $a - $b;
    }
}
