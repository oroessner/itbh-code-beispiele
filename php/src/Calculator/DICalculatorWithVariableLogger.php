<?php
declare(strict_types=1);

namespace App\Calculator;

use App\Logger\LoggerInterface;
use App\Logger\NullLogger;

/**
 * Class DICalculatorWithVariableLogger
 *
 * @package App\Calculator
 */
final class DICalculatorWithVariableLogger extends AbstractCalculator
{
    public function __construct()
    {
        $this->logger = new NullLogger();
    }

    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }
}
