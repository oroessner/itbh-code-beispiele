<?php
declare(strict_types=1);

namespace App\Calculator;

use App\Logger\LoggerInterface;

/**
 * Class DiCalculator
 *
 * @package App\Calculator
 */
final class DICalculator extends AbstractCalculator
{
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
}
