<?php
declare(strict_types=1);

namespace App\Calculator;

use App\Logger\EchoLogger;

/**
 * Class Calculator
 *
 * @package Calculator
 */
final class Calculator extends AbstractCalculator
{
    public function __construct()
    {
        $this->logger = new EchoLogger();
    }
}
