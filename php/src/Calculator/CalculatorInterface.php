<?php
declare(strict_types=1);

namespace App\Calculator;

/**
 * Class CalculatorInterface
 */
interface CalculatorInterface
{
    public function add(int $a, int $b): int;

    public function substract(int $a, int $b): int;
}
