-- Dialekt: MYSQL
CREATE TABLE IF NOT EXISTS table_league
(
    team             VARCHAR(255) NOT NULL PRIMARY KEY,
    point_difference INT          NOT NULL,
    goal_difference  INT          NOT NULL
);

TRUNCATE table_league;

INSERT INTO table_league (team, point_difference, goal_difference)
VALUES ('A', 4, 5),
       ('C', -2, -1),
       ('B', -2, -4);

-- mit FOREIGN KEY
CREATE TABLE IF NOT EXISTS table_league
(
    team_id          INT NOT NULL PRIMARY KEY,
    point_difference INT NOT NULL,
    goal_difference  INT NOT NULL,
    CONSTRAINT FOREIGN KEY (team_id) REFERENCES Mannschaft (Mannschaft_ID)
);

TRUNCATE table_league;

INSERT INTO table_league (team_id, point_difference, goal_difference)
VALUES (1, 4, 5),
       (3, -2, -1),
       (2, -2, -4);

