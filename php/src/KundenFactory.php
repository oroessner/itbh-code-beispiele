<?php
declare(strict_types=1);

namespace App;

/**
 * Class KundenFactory
 */
final class KundenFactory
{
    public function erzeugeKunde(string $typ)
    {
        switch ($typ) {
            case "kunde":
                return $this->erstelleKunde();
                break;
            case "stammkunde":
                return $this->erstelleStammkunde();
                break;
            case "mitarbeiter":
                return $this->erstelleMitarbeiter();
                break;
            default:
                throw new \RuntimeException("Ungültiger Kundentyp");
        }
    }

    public function erstelleKunde(): Kunde
    {
        return new Kunde();
    }

    public function erstelleStammkunde(): Stammkunde
    {
        return new Stammkunde();
    }

    public function erstelleMitarbeiter(): Mitarbeiter
    {
        return new Mitarbeiter();
    }
}
