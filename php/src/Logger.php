<?php
declare(strict_types=1);

namespace App;
/**
 * Class Logger
 */
final class Logger
{
    public function log(string $message)
    {
        echo $message."\n";
    }
}
