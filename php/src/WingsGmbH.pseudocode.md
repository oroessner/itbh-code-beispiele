findeRoute(gewicht: int): int
=============================

```
double letzerPreis
int resultIndex = -1;

VON index := 0 SOLANGE index < Routen.länge() {
    double routenPreis = 0.0
    int routenIndex = 1
    SOLANGE Routen[routenIndex] NOT null {
        Route route = Routen[index]
        WENN holeStreckeGewicht(route[routenIndex-1], route[routenIndex]) <= gewicht {
            routenPreis += holeStreckePreis(route[routenIndex-1], Routen[routenIndex]) * gewicht
        }
        routenIndex++
    }
    
    WENN letzterPreis IST null ODER routenPreis < letzerPreis {
        letzerPreis = routenPreis
        resultIndex = index
    } 
    
    index++
}

RETURN resultIndex
```
