<?php
declare(strict_types=1);

namespace App;


/**
 * Class ExternerMitarbeiter
 */
interface HatPersonalnummer
{
    public function getPersonalNummer(): int;
}
