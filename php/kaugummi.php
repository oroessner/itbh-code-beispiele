<?php
declare(strict_types=1);

use App\State\Kaugummiautomat;

require_once __DIR__.'/vendor/autoload.php';

$automat = new Kaugummiautomat(5);
echo "$automat\n";

$automat->muenzeEinwerfen();
$automat->griffDrehen();

echo "$automat\n";

$automat->muenzeEinwerfen();
$automat->muenzeAuswerfen();
$automat->griffDrehen();

echo "$automat\n";

