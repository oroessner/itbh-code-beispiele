package de.oroessner.kata.kotlin

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory

class ZahlensystemKonverterTest {

    @TestFactory
    internal fun testDecToHex() = listOf(
            1 to "1",
            2 to "2",
            10 to "A",
            15 to "F",
            16 to "10",
            506 to "1FA"
    ).map { (input, expected) ->
        DynamicTest.dynamicTest("convert Dec $input to Hex $expected") {
            val converter = ZahlensystemKonverter()
            assertThat(converter.decHex(input)).isEqualTo(expected)
        }
    }

    @TestFactory
    internal fun testDecToBin() = listOf(
            1 to "1",
            2 to "10",
            22 to "10110"
    ).map { (input, expected) ->
        DynamicTest.dynamicTest("convert Dec $input to Bin $expected") {
            val converter = ZahlensystemKonverter()
            assertThat(converter.decBin(input)).isEqualTo(expected)
        }
    }

    @TestFactory
    internal fun testHexToDec() = listOf(
            "1" to 1,
            "2" to 2,
            "A" to 10,
            "F" to 15,
            "10" to 16,
            "1FA" to 506
    ).map { (input, expected) ->
        DynamicTest.dynamicTest("convert Hex $input to Dec $expected") {
            val converter = ZahlensystemKonverter()
            assertThat(converter.hexDec(input)).isEqualTo(expected)
        }
    }

    @TestFactory
    internal fun testBinToDec() = listOf(
            "1" to 1,
            "10" to 2,
            "10110" to 22
    ).map { (input, expected) ->
        DynamicTest.dynamicTest("convert Bin $input to Dec $expected") {
            val converter = ZahlensystemKonverter()
            assertThat(converter.binDec(input)).isEqualTo(expected)
        }
    }
}