package de.oroessner.rechnung

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.Mockito

class RechnungsErstellerTest {
    @Test
    internal fun getKundenIds() {
        // Arrange
        val kId1 = "K00091"
        val kId2 = "K01234"
        val satz1 = "<satz-1>"
        val satz2 = "<satz-2>"

        val expected = listOf(
                kId1,
                kId2
        )

        val systemHaus = Mockito.mock(SystemHausInterface::class.java)

        Mockito.`when`(systemHaus.hole_journalsatz())
                .thenReturn(satz1)
                .thenReturn(satz2)
                .thenReturn("")

        Mockito.`when`(systemHaus.lese_kundenid(satz1)).thenReturn(kId1)
        Mockito.`when`(systemHaus.lese_kundenid(satz2)).thenReturn(kId2)

        // Act
        val rechnungsErsteller = RechnungsErsteller(systemHaus)
        // Assert
        assertThat(expected).isEqualTo(rechnungsErsteller.getKundenIds())

        Mockito.verify(systemHaus, Mockito.times(3)).hole_journalsatz()
        Mockito.verify(systemHaus, Mockito.times(2)).lese_kundenid(Mockito.anyString())
    }
}