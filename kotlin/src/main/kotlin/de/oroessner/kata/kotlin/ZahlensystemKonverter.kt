package de.oroessner.kata.kotlin

import kotlin.math.pow

class ZahlensystemKonverter {
    fun decHex(number: Int): String {
        return this.decConvert(number, 16, ::getHexNumber)
    }

    fun decBin(number: Int): String {
        return this.decConvert(number, 2, ::getRestAsString)
    }

    private fun getRestAsString(rest: Int): String {
        return rest.toString()
    }

    private fun getHexNumber(rest: Int): String {
        return if (rest > 9) {
            (rest + 55).toChar().toString() // A-F
        } else {
            rest.toString() // 0-9
        }
    }

    private fun decConvert(number: Int, base: Int, stringFactory: (rest: Int) -> String): String {
        var result = ""
        var calcNumber = number

        do {
            val rest = calcNumber % base
            result = stringFactory(rest) + result
            calcNumber /= base
        } while (calcNumber > 0)

        return result
    }

    fun hexDec(input: String): Int {
        var result = 0.0
        val chars = input.toCharArray()
        chars.reverse()

        chars.forEachIndexed { index, char ->
            result += charToDec(char, index, 16, ::hexToInt)
        }

        return result.toInt()
    }

    fun binDec(input: String): Int {
        var result = 0.0
        val chars = input.toCharArray()
        chars.reverse()

        chars.forEachIndexed { index, char ->
            result += charToDec(char, index, 2, ::charToInt)
        }

        return result.toInt()
    }

    private fun hexToInt(char: Char): Int {
        return getDecFromHex(char.toString())
    }

    private fun charToInt(char: Char): Int {
        return char.toInt() - 48
    }

    private fun charToDec(char: Char, index: Int, base: Int, intFactory: (rest: Char) -> Int): Int {
        return base.toDouble().pow(index.toDouble()).toInt() * intFactory(char)
    }

    private fun getDecFromHex(input: String): Int {
        val alpha = listOf("A", "B", "C", "D", "E", "F")
        if (alpha.contains(input)) {
            val alphaIndex = alpha.indexOf(input)
            return (alphaIndex + 65).toChar().toInt() - 55
        }

        return input.toInt()
    }


}
