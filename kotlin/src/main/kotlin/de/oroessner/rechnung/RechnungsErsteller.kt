package de.oroessner.rechnung

class RechnungsErsteller(val systemHaus: SystemHausInterface) {

    fun getKundenIds(): List<String> {
        val kunden = mutableListOf<String>()
        var satz = systemHaus.hole_journalsatz()
        while (satz != "") {
            kunden.add(systemHaus.lese_kundenid(satz))
            satz = systemHaus.hole_journalsatz()
        }

        return kunden;
    }

}
