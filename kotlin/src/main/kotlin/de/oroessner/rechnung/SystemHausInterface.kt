package de.oroessner.rechnung

interface SystemHausInterface {
    fun hole_journalsatz(): String
    fun lese_kundenid(satz: String): String
}